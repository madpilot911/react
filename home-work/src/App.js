import React, { useEffect, useState } from "react";
import { createPortal } from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import ProductList from "./Components/ProductList/ProductList";
import Modal from "./Components/Modal/Modal.js";
import modalStyles from "./Components/Modal/ModalSecond.module.scss";
import styles from "./App.module.scss";
import Button from "./Components/Button/Button.js";
import buttonStyles from "./Components/Button/Button.module.scss";
// import PropTypes from "prop-types";

const Portal = ({ children }) => {
  const mount = document.getElementById("portal-root");
  const el = document.createElement("div");

  useEffect(() => {
    mount.appendChild(el);
    return () => mount.removeChild(el);
  }, [el, mount]);

  return createPortal(children, el);
};

const Loader = () => {
  return <div className={styles.loader}>Loading...</div>;
};

function App() {
  const [items, setItems] = useState([]);
  const [isLoaded, setLoaded] = useState(false);

  const [favorites, setFavorites] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );
  const [added, setAdded] = useState(
    JSON.parse(localStorage.getItem("added")) || []
  );
  const [cartAction, setCartAction] = useState(null);

  const handle = (type, id) => {
    let arrOfId = type === "fav" ? favorites : added;
    if (!arrOfId.includes(id)) {
      arrOfId.push(id);
    } else {
      arrOfId = arrOfId.filter((item) => item !== id);
    }
    if (type === "fav") {
      localStorage.setItem("favorites", JSON.stringify(arrOfId));
      setFavorites(JSON.parse(localStorage.getItem("favorites")));
    } else if (type === "add") {
      localStorage.setItem("added", JSON.stringify(arrOfId));
      setAdded(JSON.parse(localStorage.getItem("added")));
    } else {
      throw new Error("Unknow type of action");
    }
  };
  const cartBtnHandle = (typeOfAction, id) => {
    setCartAction({
      typeOfAction,
      id,
    });
  };

  useEffect(() => {
    fetch("items.json")
      .then((res) => res.json())
      .then((res) => {
        setItems(res);
        setLoaded(true);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }, []);

  return (
    <Router>
      <div className={styles.header}>
        <nav className={styles.nav}>
          <ul className={styles.navbar}>
            <li className={styles.navbar_item}>
              <NavLink
                className={styles.navbar_link}
                activeClassName={styles.navbar_link_active}
                exact
                to="/">
                Home
              </NavLink>
            </li>
            <li className={styles.navbar_item}>
              <NavLink
                className={styles.navbar_link}
                activeClassName={styles.navbar_link_active}
                to="/favorites">
                Favorites
              </NavLink>
            </li>
            <li className={styles.navbar_item}>
              <NavLink
                className={styles.navbar_link}
                activeClassName={styles.navbar_link_active}
                to="/cart">
                Cart
              </NavLink>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/favorites">
            {isLoaded ? (
              <ProductList
                items={items}
                typeOfProductList="favorites"
                added={added}
                favorites={favorites}
                handle={handle}
                cartBtnHandle={cartBtnHandle}
              />
            ) : (
              <Loader />
            )}
          </Route>
          <Route path="/cart">
            {isLoaded ? (
              <ProductList
                items={items}
                typeOfProductList="cart"
                added={added}
                favorites={favorites}
                handle={handle}
                cartBtnHandle={cartBtnHandle}
              />
            ) : (
              <Loader />
            )}
          </Route>
          <Route exact path="/">
            {isLoaded ? (
              <ProductList
                items={items}
                typeOfProductList="all items"
                added={added}
                favorites={favorites}
                handle={handle}
                cartBtnHandle={cartBtnHandle}
              />
            ) : (
              <Loader />
            )}
          </Route>
        </Switch>
      </div>
      <Portal>
        {cartAction && (
          <Modal
            styles={modalStyles}
            closeModal={() => setCartAction(null)}
            header={
              cartAction.typeOfAction === "addToCart"
                ? "Wise Choise!"
                : "Delete this Item from Your Cart?"
            }
            text={
              cartAction.typeOfAction === "addToCart"
                ? "Press OK to add this Item to Your Cart and enjoy quality of our production"
                : "If you are sure you want to delete this from Cart press DELETE"
            }
            actions={
              <>
                <Button
                  className={buttonStyles.button}
                  background={
                    cartAction.typeOfAction === "addToCart"
                      ? "#5DADE2"
                      : "#EC7063"
                  }
                  text={
                    cartAction.typeOfAction === "addToCart" ? "Ok" : "Delete"
                  }
                  onClick={() => {
                    handle("add", cartAction.id);
                    setCartAction(null);
                  }}
                />
                <Button
                  className={buttonStyles.button}
                  background="#F5B041 "
                  text="Cancel"
                  onClick={() => {
                    setCartAction(null);
                  }}
                />
              </>
            }
          />
        )}
      </Portal>
    </Router>
  );
}

// ProductList.propTypes = {
//   styles: PropTypes.object,
//   items: PropTypes.array,
//   favorites: PropTypes.array,
//   added: PropTypes.array,
//   typeOfProductList: PropTypes.string,
//   handle: PropTypes.func,
//   cartBtnHandle: PropTypes.func,
// };

export default App;
