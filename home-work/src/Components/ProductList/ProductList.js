import Card from "../Card/Card";
import styles from "../ProductList/ProductList.module.scss";
import PropTypes from "prop-types";

export default function ProductList(props) {
  let { items, typeOfProductList, added, favorites, handle, cartBtnHandle } =
    props;
  let message;

  if (typeOfProductList === "cart") {
    const filtered = items.filter((item) => added.includes(item.code));
    items = filtered;
    message = "Your Cart is empty";
  } else if (typeOfProductList === "favorites") {
    const filtered = items.filter((item) => favorites.includes(item.code));
    items = filtered;
    message = "No Items have been added yet";
  }

  return (
    <div className={styles.wrapper}>
      {" "}
      {items.length ? (
        items.map((item) => (
          <Card
            isAdd={added.includes(item.code)}
            isFav={favorites.includes(item.code)}
            key={item.code}
            item={item}
            cartBtnHandle={cartBtnHandle}
            btnHandle={handle}
            deleteFromCartBtn={typeOfProductList === "cart"}
          />
        ))
      ) : (
        <div className={styles.empty}> {message} </div>
      )}{" "}
    </div>
  );
}

ProductList.propTypes = {
  styles: PropTypes.object,
  items: PropTypes.array,
  favorites: PropTypes.array,
  added: PropTypes.array,
  typeOfProductList: PropTypes.string,
  handle: PropTypes.func,
  cartBtnHandle: PropTypes.func,
};
