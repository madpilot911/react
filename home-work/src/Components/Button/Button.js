import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

function Button(props) {
  const { background, onClick, disabled, text } = props;
  return (
    <button
      className={styles.button}
      style={{ backgroundColor: background }}
      onClick={onClick}
      disabled={disabled && "disabled"}>
      {text}
    </button>
  );
}

Button.propTypes = {
  background: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
