import React from "react";
import PropTypes from "prop-types";

function Modal(props) {
  const { styles, header, closeButton, closeModal, text, actions } = props;
  return (
    <React.Fragment>
      <div className={styles.modal}>
        <div className={styles.modalHeader}>
          <h4>{header}</h4>
          {closeButton && (
            <button className={styles.closeBtn} onClick={closeModal}>
              &times;
            </button>
          )}
        </div>
        <div className={styles.modalBody}>
          <div>{text}</div>
        </div>
        {actions && <div className={styles.btnContainer}>{actions}</div>}
      </div>
      <div className={styles.overlay} onClick={closeModal}></div>
    </React.Fragment>
  );
}

Modal.defaultProps = {
  closeButton: true,
};

Modal.propTypes = {
  styles: PropTypes.object,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  closeModal: PropTypes.func,
};

export default Modal;
