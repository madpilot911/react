import React from "react";
import styles from "./Card.module.scss";
import Button from "../Button/Button.js";
import PropTypes from "prop-types";

function LikeIcon(props) {
  const { active } = props;
  return (
    <span className={styles.like} onClick={props.onClick}>
      <i className={active ? "fas fa-star" : "far fa-star"}> </i>{" "}
      {active ? "Remove from favorite" : "Add to favorite"}{" "}
    </span>
  );
}

function DeleteCardBtn(props) {
  return (
    <button className={styles.deleteBtn} onClick={props.onClick}>
      &times;{" "}
    </button>
  );
}

function Card(props) {
  const { name, code: id, image, price } = props.item;
  const { isFav, isAdd, deleteFromCartBtn, cartBtnHandle } = props;

  return (
    <div className={styles.card}>
      <div className={styles.img}>
        <img src={image} alt="" />
      </div>{" "}
      <LikeIcon active={isFav} onClick={() => props.btnHandle("fav", id)} />{" "}
      <div className={styles.name}> {name} </div>
      <div className={styles.purchase}>
        <div className={styles.price}> {price} </div>{" "}
        <Button
          disabled={isAdd}
          background="#34495E"
          text={isAdd ? "IN CART" : "ADD TO CART"}
          onClick={() => cartBtnHandle("addToCart", id)}
        />{" "}
      </div>{" "}
      {deleteFromCartBtn && (
        <DeleteCardBtn onClick={() => cartBtnHandle("deleteFromCart", id)} />
      )}{" "}
    </div>
  );
}

Card.propTypes = {
  code: PropTypes.string,
  image: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.string,
};

export default Card;
